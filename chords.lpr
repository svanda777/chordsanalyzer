{ Analyzátor akordů }
{ Vojtěch Švandelík, I. ročník}
{ zimní semestr 2018/2019 }
{ Programování NPRG030 }

program chords;

uses
  Crt,
  SysUtils,
  strutils;

const
  MAX_TYPES = 10; {Maximum value of types of chords, seperately minor / major}
  MAX_INVERSIONS = 5; {Maximum count of inversions per one chord}
  BLANK = 255; {Value representing error state}
  STRINGS: array[0..5] of byte = (4, 11, 7, 2, 9, 4); {Array with strings tones}
  FRET_COUNT = 20; {Fret count on one string}
  STRINGS_COUNT = 6; {String on instrument}
  FINGERS = 4; {Available fingers}
  FINGERS_SPAN = 4; {Maximum distance between fingers}

type
  TChordArray = array[0..MAX_INVERSIONS] of integer;
  TChordsInversions = array[0..1] of array[0..MAX_TYPES] of TChordArray;

  TChord = record
    keyBasic: byte;
    minor: byte; {0 = major / 1 = minor}
    chordType: integer;
  end;

  TChordTones = array[0..MAX_INVERSIONS] of byte;

  TTypes = array[0..1] of array of string;

  TFretsPlayed = array[0..STRINGS_COUNT - 1] of byte;

var
  inversions: TChordsInversions; {Table with each supported inversion}
  types: TTypes; {Table with string representing of inversions}

  {%region Loading helping files}
  {Procced help files with information about inversions}
  procedure readSingleFile(var tableInversions: TChordsInversions;
  var tableTypes: TTypes; var inputFile: Text; minor: byte);

  var
    rows: integer;
    i, j: integer;
  begin
    {Reading count of the types in file}
    ReadLn(inputFile, rows);

    {Preparing size of the types table}
    SetLength(tableTypes[minor], rows);

    for i := 0 to rows - 1 do {For each record in file}
    begin
      ReadLn(inputFile, tableTypes[minor][i]); {Save string name of inversion}
      for j := 0 to MAX_INVERSIONS do
      begin  {Saving individual tones changes}
        Read(inputFile, tableInversions[minor][i][j]);
      end;
      ReadLn(inputFile);
    end;
  end;

  {Basic tasks to prepare program - loading helping files}
  procedure preparations();
  var
    major, minor: Text;
  begin
    {Preparing files}
    Assign(major, 'major.txt');
    Assign(minor, 'minor.txt');
    reset(major);
    reset(minor);

    {Filling the tables with inversions and types}
    readSingleFile(inversions, types, major, 0);
    readSingleFile(inversions, types, minor, 1);

    {Closing the files}
    Close(major);
    Close(minor);
  end;

  {%endregion}

  {%region Converters the tones}
  {Returns tone name from tone number}
  function toneName(tone: integer): string;
  begin
    case tone of
      0: toneName := 'C';
      1: toneName := 'Db';
      2: toneName := 'D';
      3: toneName := 'Eb';
      4: toneName := 'E';
      5: toneName := 'F';
      6: toneName := 'Gb';
      7: toneName := 'G';
      8: toneName := 'Ab';
      9: toneName := 'A';
      10: toneName := 'Hb';
      11: toneName := 'H';
    end;
  end;

  {Returns tone name number from the tone name}
  function toneNumber(tone: string): byte;
  begin
    tone := lowercase(tone);
    case tone of
      'c': toneNumber := 0;
      'db', 'c#': toneNumber := 1;
      'd': toneNumber := 2;
      'eb', 'd#': toneNumber := 3;
      'e': toneNumber := 4;
      'f': toneNumber := 5;
      'gb', 'f#': toneNumber := 6;
      'g': toneNumber := 7;
      'ab', 'g#': toneNumber := 8;
      'a': toneNumber := 9;
      'hb', 'a#': toneNumber := 10;
      'h': toneNumber := 11;
    end;
  end;

  {%endregion}

  {%region Printing the tones from chord}
  {Creates the chord from key tone using the table of inversions}
  function getInversions(chord: TChord): TChordArray;
  var
    inversion, i: integer;
  begin
    for i := 0 to MAX_INVERSIONS do {for each tone in inversion}
    begin
      inversion := inversions[chord.minor][chord.chordType][i];
      if inversion = -1 then
      begin  {stop when find tone which is not included}
        getInversions[i] := -1;
        Break;
      end;

      {Get final tone}
      getInversions[i] := (chord.keyBasic + inversion) mod 12;
    end;
  end;

  {Procedure to creating better UI}
  procedure printHeading();
  begin
    ClrScr;
    WriteLn('CHORDS ANALYZER');
    WriteLn('===============');
    WriteLn;
  end;

  {Checking the entered value from user, forcing user to enter correct value}
  function letterToKeyTone(letter: string): byte;
  begin
    letter[1] := UpCase(letter[1]);

    {Letters from: C-H + A}
    while ((Ord(letter[1]) < Ord('C')) or (Ord(letter[1]) > Ord('H'))) and
      (Ord(letter[1]) <> Ord('A')) do
    begin
      WriteLn;
      Write('Enter right tone name (letter between C and H or A): ');
      ReadLn(letter);
      WriteLn;
    end;

    {Return first chord tone represented as number}
    letterToKeyTone := toneNumber(letter);
  end;

  {Print and return the result - tones in chord}
  function printChordTones(chord: TChord): TChordTones;
  var
    chordArray: TChordArray;
    tone, i, index: integer;
  begin
    {UI}
    printHeading();
    Write('The tones in entered chord are: ');

    chordArray := getInversions(chord); {Get moved tones}

    index := 0;

    {Printing the tones}
    for i := 0 to MAX_INVERSIONS do
    begin
      tone := chordArray[i];
      if tone = -1 then {stop after correct count of tones}
        Break;
      Write(toneName(tone), ' ');
      printChordTones[index] := tone; {saving the tones into array}
      Inc(index);
    end;
    for i := index to MAX_INVERSIONS do
      printChordTones[i] := BLANK; {filling not used array indexes with blank}
  end;

  {Part of the chord entering wizard}
  {User can choose a chord type from printed list}
  {Printing the list of types}
  procedure printTypes(minor: byte; key: string);
  var
    i: integer;
  begin
    {UI}
    WriteLn;
    WriteLn('Choose one of the following types: ');

    {Printing the types from predefined table}
    for i := 0 to Length(types[minor]) - 1 do
    begin
      WriteLn('  - ', i, ' = ', key, types[minor][i]);
    end;
  end;

  {Chord choosing text wizard}
  function chordNameWizard(): TChord;
  var
    temp: string;
  begin
    printHeading();

    {Minor / Major}
    Write('Enter 0 for major chord or 1 for minor: ');
    ReadLn(chordNameWizard.minor);
    WriteLn;

    {Chord key tone}
    Write('Enter the chord key (you can add # for sharp or b for a flat): ');
    ReadLn(temp);
    chordNameWizard.keyBasic := letterToKeyTone(temp);

    {Chord type}
    temp := lowercase(temp); {UI improvence}
    temp[1] := UpCase(temp[1]);
    printTypes(chordNameWizard.minor, temp);
    WriteLn;
    Write('Your choice: ');
    ReadLn(chordNameWizard.chordType);
  end;

  {Searching for type ID by user input (directly entered chord)}
  function findMatchType(input: string): TChord;
  var
    i, j: integer;
  begin
    findMatchType.keyBasic := BLANK;

    {Bruteforce all possible types}
    for j := 0 to 1 do {Both major and minor}
      for i := 0 to Length(types[j]) - 1 do {All types in category}
      begin
        if types[j][i] = input then {found match}
        begin
          findMatchType.chordType := i; {Return correct valus}
          findMatchType.minor := j;
          Exit;
        end;
      end;

    {Nothing found}
    findMatchType.chordType := -1;
    findMatchType.minor := BLANK;
  end;

  {Splitting input from user in directly entering mode}
  function proccedInput(input: string): TChord;
  var
    tempChord: TChord;
  begin
    {Deciding about key tone and removing info about that from input}
    if (Length(input) = 1) or ((input[2] <> 'b') and (input[2] <> '#')) then
    begin {Input includes only key tone}
      proccedInput.keyBasic := letterToKeyTone(input[1]);
      Delete(input, 1, 1);
    end
    else
    begin {Input has key tone with flat or sharp}
      proccedInput.keyBasic := letterToKeyTone(copy(input, 1, 2));
      Delete(input, 1, 2);
    end;

    {Checking correctness of user entered type}
    tempChord := findMatchType(input);
    while tempChord.minor = BLANK do
    begin
      writeLn;
      Write('Any chord type is not matching. Try add the type (only the type) again.');
      ReadLn(input);
    end;

    {Inserted found correct values of type to entered chord}
    proccedInput.minor := tempChord.minor;
    proccedInput.chordType := tempChord.chordType;
  end;

  {%endregion}

  {%region Convertiong the frets}
  {Convert fret to tone numer}
  function getToneByFret(fretNumber: integer): integer;
  begin
    {FRET_COUNT+1 = number of tones on one string (all frets + without fret)}
    getToneByFret := (STRINGS[fretNumber div (FRET_COUNT + 1)] +
      (fretNumber mod (FRET_COUNT + 1))) mod 12;
  end;

  {Find out string number from fret number}
  function getStringNumber(fretNumber: byte): byte;
  begin
    getStringNumber := fretNumber div (FRET_COUNT + 1);
  end;

  {Find out relative fret number on string}
  function getFretNumberOnString(fretNumber: byte): byte;
  begin
    getFretNumberOnString := fretNumber mod (FRET_COUNT + 1);
  end;

  {%endregion}

  {%region Printing the frets}
  {Not used procedure to do text print of frets in chord}
  {For testing purposes}
  procedure printPlayedFrets(var playedFrest: TFretsPlayed);
  var
    i: byte;
    stringNumber: byte;
  begin
    ReadLn;
    for i := 0 to STRINGS_COUNT - 1 do
    begin
      if playedFrest[i] = BLANK then
        break;
      stringNumber := getStringNumber(playedFrest[i]);
      Write('FretNumber: ', playedFrest[i]: 3, ' | ');
      Write('Tone: ', getToneByFret(playedFrest[i]), ' (', toneName(
        getToneByFret(playedFrest[i])), ')', ' | ');
      Write('FretString: ', getFretNumberOnString(playedFrest[i]), ' | ');
      Write('String: ', stringNumber);
      WriteLn;
    end;
    WriteLn('---------------------------------------------------------');
  end;

  {Graphic visualisation of frets in chord}
  procedure printPlayedFretsGraphics(var playedFrets: TFretsPlayed);
  var
    i, actualPosition: byte;
  begin
    actualPosition := 0;
    for i := 0 to STRINGS_COUNT * (FRET_COUNT + 1) - 1 do {each fret on instrument}
    begin
      if i mod (FRET_COUNT + 1) = 0 then {frets belonging to empty strings}
      begin
        if i <> 0 then {last - on each line}
          Write('-');
        WriteLn;
        if (actualPosition <= STRINGS_COUNT - 1) and {for empty string in chord}
          (playedFrets[actualPosition] = i) then
        begin
          Write(toneName(getToneByFret(i)), '  O||');
          Inc(actualPosition);
        end
        else if (actualPosition < STRINGS_COUNT) and {for empty string not in chord}
          (playedFrets[actualPosition] = BLANK) then
          Write(toneName(getToneByFret(i)), '  X||')
        else
          Write(toneName(getToneByFret(i)), '   ||');
        continue;
      end;
      if (actualPosition <= STRINGS_COUNT - 1) and (playedFrets[actualPosition] = i) then
      begin {marking used fret}
        Write('o|');
        Inc(actualPosition);
      end
      else {not used fret}
        Write('-|');
    end;

    {UI}
    Write('-');
    WriteLn;
    WriteLn;
    WriteLn('For continue press enter');
    ReadLn;
  end;

  {%endregion}

  {%region Finding the fingering}

  {Helping procedure for finding frets fitting in chord}
  {Setting the range in which the fret is searching}
  procedure prepareFretsRange(var fretLeft, fretRight: byte);
  var
    tempFretLeft: byte;
  begin
    {Not using any finger yet - I can choose everything I want}
    if (fretLeft = BLANK) and (fretRight = BLANK) then
    begin
      tempFretLeft := 0;
      fretRight := FRET_COUNT;
    end
    else
    begin {Preparing the borders based on fingers span}
      if fretRight - (FINGERS_SPAN - 1) < 0 then {left border}
        tempFretLeft := 0
      else
        tempFretLeft := fretRight - (FINGERS_SPAN - 1);
      if fretLeft + (FINGERS_SPAN - 1) > FRET_COUNT then {right border}
        fretRight := FRET_COUNT
      else
        fretRight := fretLeft + (FINGERS_SPAN - 1);
    end;
    fretLeft := tempFretLeft; {returning the temporary left border}
  end;

  {Output check if the frets combination is valid for chord}
  {Checking if all tones are included}
  function playedFretsCheck(var playedFrets: TFretsPlayed;
  var tonesToPlay: TChordTones): boolean;
  var
    checksum: byte;
    i, j: byte;
  begin
    checksum := 0;
    for i := 0 to MAX_INVERSIONS - 1 do {for each tone in chord find fret}
    begin
      if tonesToPlay[i] = BLANK then {blank tones}
      begin
        Inc(checksum);
        continue;
      end;
      for j := 0 to STRINGS_COUNT - 1 do {finding the right fret}
      begin
        if playedFrets[j] = BLANK then {blank frets}
          break;
        if getToneByFret(playedFrets[j]) = tonesToPlay[i] then
        begin
          Inc(checksum);
          break;
        end;
      end;
    end;
    if checksum = MAX_INVERSIONS then {decision}
      playedFretsCheck := True
    else
      playedFretsCheck := False;
  end;

  {Finding some good fret for use in chord}
  {Can be any note - important is the distance}
  function findNearestRightFret(var tonesToPlay: TChordTones;
    fretStart, countUsedFingers, fretLeft, fretRight, lastFret: byte): byte;
  var
    i, j: byte;
    nearestRightFret, minimumRightFret, stringFirstFret: byte;
  begin
    {Preparations}
    stringFirstFret := getStringNumber(fretStart) * (FRET_COUNT + 1);
    minimumRightFret := BLANK;
    prepareFretsRange(fretLeft, fretRight);

    { going through each tone in chord}
    for i := 0 to MAX_INVERSIONS - 1 do
    begin
      nearestRightFret := BLANK;
      if tonesToPlay[i] = BLANK then { skip for chords with less tones }
        break;
      if countUsedFingers < FINGERS then { choosing near tone played with everything }
      begin
        if fretStart > fretLeft + stringFirstFret then
          {when the start is not empty string}
          fretLeft := fretStart - stringFirstFret;
        for j := fretLeft to fretRight do {check each available fret in range}
          if (getToneByFret(j + stringFirstFret) = tonesToPlay[i]) and
            (lastFret <> j + stringFirstFret) then
          begin
            nearestRightFret := j;
            break;
          end;
      end
      {when used all fingers checking is there is not empty string available}
      else if (fretStart = stringFirstFret) and
        (STRINGS[getStringNumber(fretStart)] = tonesToPlay[i]) then
        nearestRightFret := 0;

      {comparing got nearest right fret with the minimum}
      if (nearestRightFret < minimumRightFret) and
        (nearestRightFret + fretStart < (FRET_COUNT + 1) * STRINGS_COUNT) then
        minimumRightFret := nearestRightFret;
    end;

    {decising between returning blank or concrete fret}
    if minimumRightFret <> BLANK then
      findNearestRightFret := stringFirstFret + minimumRightFret
    else
      findNearestRightFret := BLANK;
  end;

  {Main function to find right chord fingering}
  {Starting on the bottomest string}
  procedure findChordGraph(startFret, countTonesPlayed, usedFingers,
    fretLeft, fretRight: byte; var tonesToPlay: TChordTones; playedFrets: TFretsPlayed);
  var
    foundFret, stringNumber: byte;
    printed: boolean;
  begin
    {find nearest right fret}
    foundFret := findNearestRightFret(tonesToPlay, startFret,
      usedFingers, fretLeft, fretRight, playedFrets[countTonesPlayed]);

    printed := False; {protection for doble printing same thing}

    while foundFret <> BLANK do {if the fret is available}
    begin

      {setting the borders for next searching}
      if (foundFret mod (FRET_COUNT + 1) <> 0) and
        ((getFretNumberOnString(foundFret) < fretLeft) or (fretLeft = BLANK)) then
        fretLeft := foundFret mod (FRET_COUNT + 1);
      if (foundFret mod (FRET_COUNT + 1) <> 0) and
        ((getFretNumberOnString(foundFret) > fretRight) or (fretRight = BLANK)) then
        fretRight := foundFret mod (FRET_COUNT + 1);

      stringNumber := getStringNumber(foundFret);
      playedFrets[countTonesPlayed] := foundFret;  {saving fret for recursion}

      { Playing tone with finger - increase used fingers }
      if (foundFret mod (FRET_COUNT + 1)) <> 0 then
        Inc(usedFingers);

      { Next right tone - recursion }
      if countTonesPlayed < STRINGS_COUNT - 1 then
        findChordGraph((stringNumber + 1) * (FRET_COUNT + 1),
          countTonesPlayed + 1, usedFingers, fretLeft, fretRight,
          tonesToPlay, playedFrets)
      else if playedFretsCheck(playedFrets, tonesToPlay) = True then
      begin { Printing when reached last string }
        printPlayedFretsGraphics(playedFrets);
        printed := True;
      end;

      {find different last fret}
      foundFret := findNearestRightFret(tonesToPlay, foundFret + 1,
        usedFingers, fretLeft, fretRight, playedFrets[countTonesPlayed]);
    end;
    if (playedFretsCheck(playedFrets, tonesToPlay) = True) and (printed = False) then
      printPlayedFretsGraphics(playedFrets);
  end;

  {%endregion}

  {%region Main user interface}

  {Main function to enter the chord}
  {Returns True if user wants to continue or False to quit the program}
  function chordAnalyzer(): boolean;
  var
    input: string;
    chord: TChord;
    chordTones: TChordTones;
    basicFretsPlayed: TFretsPlayed = (BLANK, BLANK, BLANK, BLANK, BLANK, BLANK);
  begin
    {UI menu}
    WriteLn('Menu:');
    WriteLn('  - enter a chord name for analysing directly (you should know the notation)');
    WriteLn('  - enter 1 for chord name wizard');
    WriteLn('  - enter 0 for quit');
    WriteLn;
    Write('Your choice: ');
    ReadLn(input);

    if input = '0' then {quit the program}
    begin
      chordAnalyzer := False;
      Exit;
    end
    else if input = '1' then {launch the wizard}
      chord := chordNameWizard()
    else {work with directly entered input}
      chord := proccedInput(input);

    {Print the tones in chord}
    chordTones := printChordTones(chord);
    WriteLn;
    WriteLn;
    WriteLn('There are some possible fingerings:');
    findChordGraph(0, 0, 0, 255, 255, chordTones, basicFretsPlayed);
    WriteLn('There is no other chord fingering. Press enter to go back to main menu.');
    ReadLn;

    {Start new lifecycle of program - reopen menu}
    printHeading();
    chordAnalyzer := True;
  end;

  {%endregion}

var
  choice: boolean;
begin
  {Welcoming}
  WriteLn('HELLO! WELCOME TO CHORDS ANALYZER.');
  WriteLn('==================================');
  WriteLn;

  {Loading helping files}
  preparations();

  {Lifecycle}
  repeat
    choice := ChordAnalyzer();
  until not choice;
end.
