# Analyzátor akordů

Toto je dokumentace k zápočtovému programu vzniklému v rámci předmětu Programování 1 na MFF UK v akademickém roce 2018/2019.

**Vedoucí práce**: Bc. Vladislav Vancák

## Zadání

Předmětem programu bylo vytvořit program v jazyce *Pascal*, který by zvládal jednoduchou analýzu kytarových akordů. Program by měl mít dvě hlavní části:

1. vypsání tónů, ze kterých se akord skládá
2. navržení několika různých prstokladů, které lze použít pro zahrání daného akordu na kytaře

## Program

Program pracuje hlavně s myšlenkou, že jednotlivé tóny lze vyjádřit pomocí čísel od 0 do 11. Konkrétně čísla odpovídají následujícím tónům: *C, C#, D, D#, E, F, F#, G, G#, A, A#, H*. Z tohoto systému je také patrné, že jednotlivé vztahy mezi tóny lze vyjádřit jako nějaké číslo popisující rozdíl mezi čísly konkrétních tónů. Tato myšlenka velice pomůže první části programu, protože si u každého typu akordu můžeme říct, jaké posuny v akordu nastávají.

Kromě samotného zdrojového kódu program také obsahuje dva soubory (*major.txt* a *minor.txt*), které právě obsahují číselně zapsané posuny v jednotlivých typech durových, resp. molových akordů. Tyto soubory obsahují některé základní akordy a není problém je dále rozšiřovat. Program se vždy načte novou verzi akordů při každém spuštění.

### Struktura pomocných souborů

Každý ze souborů má následující strukturu:

   - první řádek obsahuje počet typů akordů v souboru obsažených
   - následuje posloupnost dvojic řádků, kdy vždy na prvním řádku je název akordu (bez označení tóniny) a po názvem jsou uvedeny jednotlivé posuny tónů v akordu vůči základnímu tónu (prvnímu tónu tóniny)

### Analýza tónů akordu

Analýza tónů probíhá jednoduše na základně zvolené tóniny a druhu akordu. Jednotlivé tóny se vypočítávají na základně prostých matematických výpočtů.

### Návrh prstokladu akordu

Program navrhuje jednotlivé prstoklady na základě rekurznivního zkoušení veškerých možností. Funkce obsluhující tuto událost zohledňuje mimojiné i maximální možnou vzdálenost mezi prsty a nepoužívání více prstů, než je u běžných osob normální. Program má naimplementované dvě funkce na vypisování takto navržených prstokladů - jeden textový (aktuálně nepoužíváný) a jeden grafický, který vychází z běžného značení akordů.

### Konfigurace programu

Program lze konfigurovat před jeho spuštěním pomocí programovových konstakt v horní části programu. Tyto konstanty nám umožňují přizpůsobit program našim potřebám. Konkrétně nám umožňují nastavit maxiální rozpětí prstů (vhodné pro osoby s menším rozpětím než je autorovo), dále počet strun nástroje (praktické, pokud hrajeme na speciální typy kytar) a s tím související definování tónů jednotlivých strun. Osoby s fyzickým poškozením prstů si mohou také zvolit, aby program používal jiný počet prstů než je běžné.

### Uživatelské prostředí

Program je navržen tak, aby fungoval ve smyčce, dokud si bude uživatel přát. Obsahuje několik variant fungování (viz uživatelský návod).

## Technický popis programu

Samotný program obsluhuje funkce `ChordAnalyzer`. Před touto funkcí jsou prováděny pouze přípravy pro program (jako například načtení externích pomocných souborů). Jednotlivé položky menu obsluhují zvláštní funkce, které mají za úkol získat od uživatele patřičné zadání, které potom předají do společné funkce, která zadání zpracuje. Pro přímé zadání akordu se jedná o funkci `proccedInput`, která zadaný řetězec rozdělí na jednotlivé části dle významu a zkontroluje, jestli jsou validní. Pro akordového wizarda je to funkce `chordNameWizard`, která pomocí postupných otázek zjišťuje o jaký akord má uživatel zájem.

O vypsání prstokladu se stará rekurznivní funkce `findChordGraph`.

Zjištěný akord se ukládá do datové struktury `TChordTones`, která obsahuje jednotlivé tóny akordu. Prázdná nevyužitá místa jsou zaplněny konstantou `BLANK`, která značí prázdné místo.

Prstoklad akordu je uložen ve struktuře `TFretsPlayed`, která obsahuje konkrétní čísla pražců, které jsou součástí možného prstokladu. I zde jsou volná místa označeny konstantou `BLANK`.

## Uživatelský návod

Po spušení programu se otevře základní prostředí programu. Zde si uživatel může zvolit, který akord by rád analyzoval. Jsou k tomu připravena dvě prostředí.

První prostředí je pro zkušené uživatele programu, které znají značení jednotlivých typů akordů. Těmto uživatelům potom jen stačí napsat přímo do vstupu programu název akordu, který je zajímá. Název akordu se skládá z tóniny (název tóniny může obsahovat i křížky) a typu akordu dle popisů jednotlivých typů akordů v přiložených souborech.

Po zadání názvu akordu se vypíšou tóny, ze kterých se akord skládá a také jednotlivé návrhy na prstoklad. Zpět do úvodního menu se uživatel dostane po stisku libovolné klávesy.

Druhé prostředí je určeno pro uživatele začátečníky, kteří se s programem teprve seznamují. Po spušení programu mohou zadat číslici 1 a potvrdit ji klávesou enter. Touto volbou bude spuštěn průvodce výběru akordu.

Jako první je uživatel vyzván, aby vybral, jestli chce analyzovat durový nebo molový akord. Po zvolení této možnosti musí uživatel zadat tóninu, která ho zajímá. Po zadání tóniny se vypíšou všechny druhy zvoleného typu akordu, ze kterých si může uživatel vybrat na základě číselného kódu. Jednotlivé popisy akordů odpovídájí kódům, které mohou uživatelé zadávat v části pro zkušené uživatele.

Po výběru konkrétního akordu se zobrazí tóny v něm obsažené a první návrh prstokladu. Stiskem klávesy enter si uživatel může zobrazit další prstoklady. Toto se opakuje dokud jsou nějaké prstoklady možné. Potom se po stisku libovolné klávesy program navrátí do hlavního menu.

Tento cyklus probíhá dokud uživatel v hlavním menu nezadá klávesu 0. V tomto případě program skončí.

## Průběh prací

Práce na programu probíhaly od píky, protože většina serverů, které se věnují akordům a hudbě obecně je zkoumá spíše z hudebního hlediska oproti mnou požadovanému matematickému hledisku pro lepší a systematickou práci s akordy.

## Kam dál

Program obsahuje celou řadu možných vylepšení, z nichž některá uvádím dále:

  - možnost konfigurace přímo ve spuštěném programu
  - uživatelské prostředí pro přidávání dalších typů akordů
  - podpora pro transponování
  - zohlednění dalších faktorů při generování prstokladu

## Závěr

Práce na programu mne bavila a doufám, že budu mít v budoucnu kapacity ho dále rozvíjet.
